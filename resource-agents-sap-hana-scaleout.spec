#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the SAPHanaSR-ScaleOut upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="SAPHanaSR-ScaleOut-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global upstream_prefix ClusterLabs-resource-agents
%global upstream_version e76b7d3a

%global saphana_scaleout_prefix SAPHanaSR-ScaleOut
%global saphana_scaleout_hash b2c5218

# Whether this platform defaults to using systemd as an init system
# (needs to be evaluated prior to BuildRequires being enumerated and
# installed as it's intended to conditionally select some of these, and
# for that there are only few indicators with varying reliability:
# - presence of systemd-defined macros (when building in a full-fledged
#   environment, which is not the case with ordinary mock-based builds)
# - systemd-aware rpm as manifested with the presence of particular
#   macro (rpm itself will trivially always be present when building)
# - existence of /usr/lib/os-release file, which is something heavily
#   propagated by systemd project
# - when not good enough, there's always a possibility to check
#   particular distro-specific macros (incl. version comparison)
%define systemd_native (%{?_unitdir:1}%{!?_unitdir:0}%{nil \
  } || %{?__transaction_systemd_inhibit:1}%{!?__transaction_systemd_inhibit:0}%{nil \
  } || %(test -f /usr/lib/os-release; test $? -ne 0; echo $?))

# determine the ras-set to process based on configure invokation
%bcond_with rgmanager
%bcond_without linuxha

Name:		resource-agents-sap-hana-scaleout
Summary:	SAP HANA Scale-Out cluster resource agents
Epoch:		1
Version:	0.164.2
Release:	2%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}.1
License:	GPLv2+
URL:		https://github.com/SUSE/SAPHanaSR-ScaleOut
Source0:	%{upstream_prefix}-%{upstream_version}.tar.gz
Source1:	%{saphana_scaleout_prefix}-%{saphana_scaleout_hash}.tar.gz

BuildArch:	noarch

# Build dependencies
BuildRequires: make
BuildRequires: automake autoconf pkgconfig gcc
BuildRequires: perl-interpreter
BuildRequires: libxslt glib2-devel
BuildRequires: systemd
BuildRequires: which

%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
BuildRequires: python3-devel
%else
BuildRequires: python-devel
%endif

%if 0%{?fedora} || 0%{?centos} || 0%{?rhel}
BuildRequires: docbook-style-xsl docbook-dtds
%if 0%{?rhel} == 0
BuildRequires: libnet-devel
%endif
%endif

%if 0%{?suse_version}
BuildRequires:  libnet-devel
BuildRequires:  libglue-devel
BuildRequires:  libxslt docbook_4 docbook-xsl-stylesheets
%endif

Requires:	resource-agents >= 4.8.0
Conflicts:	resource-agents-sap-hana

Requires: /bin/bash /usr/bin/grep /bin/sed /bin/gawk
Requires: perl

%description
The SAP HANA Scale-Out resource agents interface with Pacemaker
to allow SAP HANA Scale-Out instances to be managed in a cluster
environment.

%prep
%setup -q -n %{upstream_prefix}-%{upstream_version}
%setup -q -T -D -a 1 -n %{upstream_prefix}-%{upstream_version}

# add SAPHana agents to Makefile.am
mv %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/ra/SAPHana* heartbeat
sed -i -e '/			SAPInstance		\\/a\			SAPHanaController			\\\n			SAPHanaTopology		\\' heartbeat/Makefile.am
sed -i -e '/                          ocf_heartbeat_SAPInstance.7 \\/a\                          ocf_heartbeat_SAPHanaController.7 \\\n                          ocf_heartbeat_SAPHanaTopology.7 \\' doc/man/Makefile.am

cp %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/doc/LICENSE .

%build
if [ ! -f configure ]; then
	./autogen.sh
fi

%if 0%{?fedora} >= 11 || 0%{?centos} > 5 || 0%{?rhel} > 5
CFLAGS="$(echo '%{optflags}')"
%global conf_opt_fatal "--enable-fatal-warnings=no"
%else
CFLAGS="${CFLAGS} ${RPM_OPT_FLAGS}"
%global conf_opt_fatal "--enable-fatal-warnings=yes"
%endif

%if %{with rgmanager}
%global rasset rgmanager
%endif
%if %{with linuxha}
%global rasset linux-ha
%endif
%if %{with rgmanager} && %{with linuxha}
%global rasset all
%endif

export CFLAGS

%configure \
%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
	PYTHON="%{__python3}" \
%endif
	%{conf_opt_fatal} \
%if %{defined _unitdir}
    --with-systemdsystemunitdir=%{_unitdir} \
%endif
%if %{defined _tmpfilesdir}
    --with-systemdtmpfilesdir=%{_tmpfilesdir} \
    --with-rsctmpdir=/run/resource-agents \
%endif
	--with-pkg-name=resource-agents \
	--with-ras-set=%{rasset}

%if %{defined jobs}
JFLAGS="$(echo '-j%{jobs}')"
%else
JFLAGS="$(echo '%{_smp_mflags}')"
%endif

make $JFLAGS

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# remove other agents
find %{buildroot}/usr/lib/ocf ! -type d ! -iname "SAPHana*" -exec rm {} \;
find %{buildroot}/%{_mandir} -type f ! -iname "*SAPHana*" -exec rm {} \;

install -m 0755 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/bin/{SAPHanaSR-monitor,SAPHanaSR-showAttr} %{buildroot}/%{_sbindir}
mkdir %{buildroot}/%{_usr}/lib/SAPHanaSR-ScaleOut
install -m 0444 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/test/SAPHanaSRTools.pm %{buildroot}/%{_usr}/lib/SAPHanaSR-ScaleOut/SAPHanaSRTools.pm
mkdir -p %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut/samples
install -m 0644 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/srHook/SAPHanaSR.py %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut
install -m 0444 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/crmconfig/* %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut/samples
install -m 0444 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/srHook/global.ini %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut/samples
gzip %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/man/SAPHanaSR*.?
cp %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/man/SAPHanaSR*.7.gz %{buildroot}/%{_mandir}/man7
cp %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/man/SAPHanaSR*.8.gz %{buildroot}/%{_mandir}/man8

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%files
%defattr(-,root,root)
%license LICENSE
%{_usr}/lib/ocf/resource.d/heartbeat/SAPHana*
%{_mandir}/man7/*SAPHana*
%{_mandir}/man8/*SAPHana*
%{_sbindir}/SAPHanaSR*
%{_usr}/lib/SAPHanaSR-ScaleOut
%{_datadir}/SAPHanaSR-ScaleOut

%exclude /etc
%exclude /usr/include
%exclude /usr/lib/debug
%exclude /usr/lib/systemd
%exclude /usr/lib/tmpfiles.d
%exclude /usr/libexec
%exclude /usr/sbin/ldirectord
%exclude /usr/sbin/ocf*
%exclude /usr/share/resource-agents
%exclude /usr/share/pkgconfig/resource-agents.pc

%exclude /usr/src

%changelog
* Tue Aug 10 2021 Mohan Boddu <mboddu@redhat.com> - 1:0.164.2-2.1
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Mon Jun 14 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.164.2-2
- Add CI gating tests

  Resolves: rhbz#1958970

* Mon Jun  7 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.164.2-1
- Rebase to SAPHanaSR-ScaleOut 0.164.2 upstream release.

* Thu Apr 30 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.164.0-1
- Set default timeouts based on recommendations and a couple of bugfixes

  Resolves: rhbz#1827107

* Tue Feb 18 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.163.2-6
- Add Conflicts: to avoid future CI gating errors

  Resolves: rhbz#1802995

* Thu Jun 27 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.163.2-5
- Initial build as separate package

  Resolves: rhbz#1705765
